def testBranch(){
    echo 'Testing the branch ...'
    echo "Executing the pipeline for $BRANCH_NAME branch ..."
}

def buildJar(){
    echo 'Building the .jar file ...'
    sh 'mvn package'
}

def buildImageAndPush(){
    echo 'Building the docker image ...'
    withCredentials([usernamePassword(credentialsId: 'dockerhub-credentials', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
        sh 'docker build -t javidjhuseyn/maven-test-app:V-7.0 .'
        sh "echo $PASSWORD | docker login -u $USERNAME --password-stdin"
        sh 'docker push javidjhuseyn/maven-test-app:V-7.0'
    }
}

def testStep(){
    echo 'Just Testing ...'
}
return this
